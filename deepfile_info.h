//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_INFO_H
#define DEEPFILE_INFO_H

#define DEEPFILE_APP_VER 20200117
#define DEEPFILE_DB_VER 20200117

#define DEEPFILE_BUILD_TIME __TIME__
#define DEEPFILE_BUILD_DATE __DATE__

#endif //DEEPFILE_INFO_H
