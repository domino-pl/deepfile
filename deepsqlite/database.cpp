//
// Created by Dominik Przybysz.
//

#include "database.h"
#include "query.h"
#include "errors.h"

using namespace dsqlite;

void Database::transaction_begin() {
    this->exec("BEGIN");
}

void Database::transaction_commit() {
    this->exec("COMMIT");
}

void Database::transaction_rollbeck() {
    this->exec("ROLLBACK");
}

void Database::set_synchronous(const Database::synchronous_level & level) {
    switch(level){
        case SYNC_OFF:
            this->exec("PRAGMA synchronous=OFF");
        break;
        case SYNC_NORMAL:
            this->exec("PRAGMA synchronous=NORMAL");
        break;
        case SYNC_FULL:
            this->exec("PRAGMA synchronous=FULL");
        break;
        case SYNC_EXTRA:
            this->exec("PRAGMA synchronous=EXTRA");
        break;
    }


}

int64_t Database::last_inserted_row_id() const {
    return sqlite3_last_insert_rowid(this->db);
}

int Database::changed_rows() const {
    return sqlite3_changes(this->db);
}

sqlite_code_t Database::error_code() const {
    return sqlite3_errcode(this->handler());
}

std::string &Database::error_msg(std::string &msg) const {
    msg = sqlite3_errmsg(this->handler());
    return msg;
}

std::string Database::error_msg() const {
    std::string msg;
    return this->error_msg(msg);
}

void Database::open(const std::string &path, bool create) {
    int flags = SQLITE_OPEN_READWRITE;
    if(create)
        flags |= SQLITE_OPEN_CREATE; // NOLINT(hicpp-signed-bitwise)
    sqlite_code_t result = sqlite3_open_v2(path.c_str(), &(this->db), flags, nullptr);

    if(result != SQLITE_OK){
        std::string msg;
        if(result == SQLITE_NOMEM)
            msg = "No enough memory";
        else
            msg = this->error_msg();
        throw dsqlite::errors::SqliteError(result, msg);
    }else
        this->ready = true;
}

void Database::close() {
    sqlite3_close_v2(this->db);
    this->ready = false;
}

void Database::exec(const char *query) {
    bool result = sqlite3_exec(this->db, query, nullptr, nullptr, nullptr);
    if(result != SQLITE_OK)
        throw dsqlite::errors::SqliteError(result, this->error_msg());
}

void Database::exec(std::string query) {
    this->exec(query.c_str());
}

