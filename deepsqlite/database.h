//
// Created by Dominik Przybysz.
//

#ifndef DEEPSQLITE_DATABASE_H
#define DEEPSQLITE_DATABASE_H

#include <string>
#include <cstring>
#include <ostream>

#include "../thirdparty/sqlite-amalgamation/sqlite3.h"

#include "types.h"


namespace dsqlite {

    class Database{
    public:
        enum synchronous_level{SYNC_OFF=0, SYNC_NORMAL=1, SYNC_FULL=2, SYNC_EXTRA=3};

    protected:
        /// Pointier to sqlite database connection object
        sqlite3 * db = nullptr;
        /// True if database is opened
        bool ready = false;

    public:
        /// \param path - path to database file
        /// \param create - create database file if it doesn't exist
        virtual void open(const std::string & path, bool create = false);
        /// Close connection to database file
        void close();
        /// \return - True if database is opened
        inline bool is_ready(){
            return this->ready;
        }

    public:
        /// \return - handler to inner sqlite database connection object
        [[nodiscard]] inline sqlite3 * handler() const{
            return this->db;
        }

    public:
        /// Begin transaction - throw exception if transaction has been begun already
        void transaction_begin();
        /// Commit transaction - throw exception if transaction hasn't been begun yet
        void transaction_commit();
        /// Rollbeck transaction - throw exception if transaction hasn't been begun yet
        void transaction_rollbeck();

    public:
        /// Will throw exception called inside transaction
        void set_synchronous(const synchronous_level & level);

    public:
        /// \return - last inserted row id
        [[nodiscard]] int64_t last_inserted_row_id() const;
        /// \return - number of rows modified by last query
        [[nodiscard]] int changed_rows() const;

    public:
        /// \return - error code returned by last query
        [[nodiscard]] sqlite_code_t error_code() const;
        /// \param msg - std::string which will contain returned message
        /// \return - error message returned by last query/action
        std::string & error_msg(std::string & msg) const;
        /// \return - error message returned by last query/action
        [[nodiscard]] std::string error_msg() const;

    public:
        /// Execute simple query
        /// \param query - query which will be executed
        void exec(const char * query);
        /// Execute simple query
        /// \param query - query which will be executed
        void exec(std::string query);
    };
}

#endif //DEEPSQLITE_DATABASE_H
