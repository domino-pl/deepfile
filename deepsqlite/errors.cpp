//
// Created by Dominik Przybysz.
//

#include "errors.h"

using namespace dsqlite;

errors::SqliteError::SqliteError(const sqlite_code_t &code, std::string sqlite_msg)
: code(code), sqlite_msg(std::move(sqlite_msg)){
}

std::string errors::SqliteError::details() const noexcept {
    std::string to_return = DeepException::details();
    to_return += "\n";
    to_return += "Code: ";
    to_return += std::to_string(this->code);
    to_return += " (";
    to_return += sqlite3_errstr(this->code);
    to_return += ")";
    to_return += "\n";
    to_return += this->sqlite_msg;

    return to_return;
}

errors::BindError::BindError(const sqlite_code_t &code, std::string sqlite_msg, StatementQuery::BindType bind_type,
                             parameter_number_t parameter)
                             : SqliteError(code, std::move(sqlite_msg)), bind_type(bind_type), parameter(parameter){

}

std::string errors::BindError::details() const noexcept {
    std::string to_return = BindError::SqliteError::details();
    to_return += "\n";
    to_return += "Bind type ";
    to_return += std::to_string(this->bind_type);
    to_return += " - filed ";
    to_return += std::to_string(this->parameter);
    return to_return;
}
