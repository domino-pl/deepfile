//
// Created by Dominik Przybysz.
//

#ifndef DEEPSQLITE_ERRORS_H
#define DEEPSQLITE_ERRORS_H

#include <string>

#include "../thirdparty/sqlite-amalgamation/sqlite3.h"

#include "../deeptools/exceptions.h"
#include "query.h"
#include "types.h"



namespace dsqlite::errors{

    class SqliteError : public dexception::DeepException{
        protected:
            /// Sqlite error code
            const sqlite_code_t code;
            /// Sqlite error message
            const std::string sqlite_msg;
        public:
            explicit SqliteError(const sqlite_code_t & code, std::string sqlite_msg = "");
            [[nodiscard]] const char * what() const noexcept override{
                return "Sqlite error";
            }
            [[nodiscard]] std::string details() const noexcept override;
        };

        class BindError : public virtual SqliteError{
        protected:
            /// Source parameter number
            const parameter_number_t parameter;
            /// Bind type
            const dsqlite::StatementQuery::BindType bind_type;
        protected:
            [[nodiscard]] const char * what() const noexcept override{
                return "Bind error";
            }
        public:
            BindError(const sqlite_code_t & code, std::string sqlite_msg, StatementQuery::BindType bind_type, parameter_number_t parameter);
            [[nodiscard]] std::string details() const noexcept override;
        };
}

#endif //DEEPFILE_ERRORS_H
