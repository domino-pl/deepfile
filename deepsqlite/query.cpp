//
// Created by Dominik Przybysz.
//

#include "query.h"

#include "errors.h"

using namespace dsqlite;

StatementQuery::StatementQuery(Database & db, const std::string & query){
    this->prepare(db, query);
}

void StatementQuery::finalize() {
    sqlite3_finalize(this->stmt);
}

void StatementQuery::bind(const parameter_number_t &parameter, const char *value, const size_t &len) {
    this->bind_result = sqlite3_bind_text(this->stmt, parameter, value, (int) len, SQLITE_TRANSIENT);
    this->bind_result_exception_throw(BindType::BIND_TEXT, parameter);
}

void StatementQuery::bind(const parameter_number_t &parameter, const char *value) {
    this->bind(parameter, value, strlen(value));
}

void StatementQuery::bind(const parameter_number_t &parameter, const std::string &value) {
    this->bind(parameter, value.c_str(), value.length());
}

void StatementQuery::bind_null(const parameter_number_t &parameter) {
    this->bind_result = sqlite3_bind_null(this->stmt, parameter);
    this->bind_result_exception_throw(BindType::BIND_NULL, parameter);
}

const char *StatementQuery::sql_raw() const {
    return sqlite3_sql(this->stmt);
}

const char *StatementQuery::sql() const {
    return sqlite3_expanded_sql(this->stmt);
}

void StatementQuery::bind_result_exception_throw(const BindType &bind_type, const parameter_number_t &column) const {
    if(this->bind_result != SQLITE_OK)
        throw errors::BindError(this->bind_result, this->db->error_msg(), bind_type, column);
}

QueryResult StatementQuery::exec_read() {
    sqlite3_reset(this->stmt);
    this->step_result = sqlite3_step(this->stmt);

    this->exec_result = this->step_result;
    if(this->exec_result != SQLITE_DONE && this->exec_result != SQLITE_ROW){
        throw errors::SqliteError(this->exec_result, this->db->error_msg());
    }

    return QueryResult(*this);
}

void StatementQuery::prepare(dsqlite::Database &db, const std::string &query) {
    this->db = &db;
    sqlite3_finalize(this->stmt);
    sqlite3_prepare_v2(this->db->handler(), query.c_str(), -1, &(this->stmt), nullptr);
}

void StatementQuery::exec(bool clear_bindings) {
    sqlite3_reset(this->stmt);
    this->step_result = sqlite3_step(this->stmt);

    this->exec_result = this->step_result;
    if(this->exec_result != SQLITE_DONE && this->exec_result != SQLITE_ROW){
        throw errors::SqliteError(this->exec_result, this->db->error_msg());
    }

    if(clear_bindings)
        sqlite3_clear_bindings(this->stmt);
    sqlite3_reset(this->stmt);
}

bool QueryResult::next() {
    this->query->step_result = sqlite3_step(this->query->stmt);

    if(this->query->step_result != SQLITE_DONE && this->query->step_result != SQLITE_ROW){
        throw errors::SqliteError(this->query->step_result, this->query->db->error_msg());
    }

    return this->query->step_result == SQLITE_ROW;
}

QueryResult::QueryResult(StatementQuery &query) : query(&query){

}

DsqliteType QueryResult::get_type(const column_number_t &column) {
    // TODO: check no record and no column in all get_* methods
    auto to_return = static_cast<DsqliteType>(sqlite3_column_type(this->query->stmt, column));
    return to_return;
}

int32_t QueryResult::get_int32(const column_number_t &column) const {
    int32_t to_return = sqlite3_column_int(this->query->stmt, column);
    return to_return;
}

int64_t QueryResult::get_int64(const column_number_t &column) const {
    int64_t to_return = sqlite3_column_int64(this->query->stmt, column);
    return to_return;
}

double QueryResult::get_double(const column_number_t &column) {
    double to_return = sqlite3_column_double(this->query->stmt, column);
    return to_return;
}

std::string &QueryResult::get_string(const column_number_t &column, std::string &output) {
    const char * to_return = (const char *)sqlite3_column_text(this->query->stmt, column);
    output = std::string(to_return);
    return output;
}

std::string QueryResult::get_string(const column_number_t &column) {
    std::string to_return;
    this->get_string(column, to_return);
    return to_return;
}

bool QueryResult::row() {
    return (this->query->step_result == SQLITE_ROW);
}

void QueryResult::finish(bool clear_bindings) {
    sqlite3_reset(this->query->stmt);
    if(clear_bindings)
        sqlite3_clear_bindings(this->query->stmt);
}

bool QueryResult::get_is_null(const column_number_t &column) {
    return this->get_type(column) == dsqlite::DsqliteType::DSQLITE_NULL;
}

QueryResult::QueryResult() : query(nullptr){

}
