//
// Created by Dominik Przybysz.
//

#ifndef DEEPSQLITE_QUERY_H
#define DEEPSQLITE_QUERY_H

#include <string>

#include "../thirdparty/sqlite-amalgamation/sqlite3.h"

#include "types.h"
#include "database.h"


namespace dsqlite{

    class QueryResult;

    class StatementQuery {
        /// Object allowing to read data from database after executing query
        friend QueryResult;
    public:
        /// Sqlite3 bindings types
        enum BindType{BIND_INT, BIND_INT64, BIND_TEXT, BIND_NULL};

    protected:
        // Pointer to database connection object
        Database * db = nullptr;

        /// Pointier to sqlite statement object
        sqlite3_stmt * stmt = nullptr;

        /// Last step(exec/exec_read/QueryResult::next) result
        sqlite_code_t step_result = SQLITE_OK;
        /// Last bind result code
        sqlite_code_t bind_result = SQLITE_OK;
        /// Last exec result code
        sqlite_code_t exec_result = SQLITE_OK;

    protected:
        /// Throw BindError exception according to last binding result this->bind_result
        /// \param bind_type - bind type to report
        /// \param column - column id to report
        void bind_result_exception_throw(const BindType &bind_type, const parameter_number_t &column) const;

    public:
        /// Query string might be specified in constructor or after using prepare method
        StatementQuery() = default;
        /// Query string might be specified in constructor or after using prepare method
        /// \param db - database connection object
        /// \param query - query string
        explicit StatementQuery(Database &db, const std::string &query);
        /// Query string might be specified in constructor or after using prepare method
        /// \param db - database connection object
        /// \param query - query string
        void prepare(Database &db, const std::string &query);
        /// Finalize query (release memory)
        void finalize();

    public:
        /// Return pointier to query statement
        inline sqlite3_stmt * statement(){
            return this->stmt;
        }

    public:
        /// \return Raw sql query as it was passed during object preparing.
        [[nodiscard]] const char *sql_raw() const;
        /// \return Sql query with currently bind values.
        [[nodiscard]] const char *sql() const;

    public:
        /// Execute prepared query - not bound parameters will be replaced with NULL
        /// \param clear_bindings - if true bound parameters will be cleared after query execution
        void exec(bool clear_bindings = true);
        /// Execute prepared query and return object which allows to read returned records
        QueryResult exec_read();

    public:
        /// \tparam T - auto detected numerical type where sizeof(T)<=sizeof(int)
        /// \param parameter - number of parameter in sql query (?NNN)
        /// \param value - value to bind
        template<
                typename T, //real type
                typename = typename std::enable_if<std::is_integral<T>::value, T>::type,
                typename = typename std::enable_if<(sizeof(T) <= sizeof(int)), T>::type
        >
        void bind(parameter_number_t parameter, const T &value) {
            this->bind_result = sqlite3_bind_int(this->stmt, parameter, value);
            this->bind_result_exception_throw(BindType::BIND_INT, parameter);
        }
        /// \tparam T - auto detected numerical type where sizeof(T)>sizeof(int)
        /// \param parameter - number of parameter in sql query (?NNN)
        /// \param value - value to bind
        template<
                typename T, //real type
                typename = typename std::enable_if<std::is_integral<T>::value, T>::type,
                typename = typename std::enable_if<(sizeof(T) > sizeof(int)), T>::type,
                typename = void
        >
        void bind(const parameter_number_t &parameter, const T &value) {
            this->bind_result = sqlite3_bind_int64(this->stmt, parameter, value);
            this->bind_result_exception_throw(BindType::BIND_INT64, parameter);
        }
        /// \param parameter - number of parameter in sql query (?NNN)
        /// \param value - value to bind
        void bind(const parameter_number_t &parameter, const char *value, const size_t &len);
        /// \param parameter - number of parameter in sql query (?NNN)
        /// \param value - value to bind
        void bind(const parameter_number_t &parameter, const char *value);
        /// \param parameter - number of parameter in sql query (?NNN)
        /// \param value - value to bind
        void bind(const parameter_number_t &parameter, const std::string &value);
        /// \param parameter - number of parameter in sql query (?NNN)
        /// \param value - value to bind
        void bind_null(const parameter_number_t &parameter);
    };


    class QueryResult{
    protected:
        /// Pointer to query which result represents this object
        StatementQuery * query;

    public:
        QueryResult();;
        /// \param query - query which result represents this object
        explicit QueryResult(StatementQuery & query);

    public:
        /// This method should be called before any other reading column result in order to correctly detect type
        /// \param column - result column number
        /// \return Sqlite column type
        DsqliteType get_type(const column_number_t & column);
        /// This method should be called before any other reading column result in order to correctly detect type
        /// \param column - result column number
        /// \return true if checked column contains NULL
        bool get_is_null(const column_number_t & column);
        /// \param column - result column number
        /// \return column contents converted to int32_t
        [[nodiscard]] int32_t get_int32(const column_number_t & column) const;
        /// \param column - result column number
        /// \return column contents converted to int64_t
        [[nodiscard]] int64_t get_int64(const column_number_t & column) const;
        /// \param column - result column number
        /// \return column contents converted to double
        double get_double(const column_number_t & column);
        /// \param column - result column number
        /// \param output - string which will store column contents
        /// \return column contents converted to string
        std::string & get_string(const column_number_t & column, std::string & output);
        /// \param column - result column number
        /// \return column contents converted to string
        std::string get_string(const column_number_t & column);

    public:
        /// \return - true if there are any data to read (record exists)
        bool row();

        /// Step to next record
        /// \return true if next record exists
        bool next();

        /// Finish reading data from results
        /// \param clear_bindings - clear bound parameters in executed query
        void finish(bool clear_bindings = true);
    };

}

#endif //DEEPFILE_QUERY_H
