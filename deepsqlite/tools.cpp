//
// Created by Dominik Przybysz.
//

#include <regex>

#include "../thirdparty/sqlite-amalgamation/sqlite3.h"

#include "tools.h"

std::string dsqlite::escape(const std::string &input){
    const static std::regex expression ("'");
    return std::string("'") + std::regex_replace (input, expression , "''") + '\'';
}

std::string &dsqlite::error_name(const dsqlite::sqlite_code_t &error_code, std::string &error_name_output) {
    error_name_output = sqlite3_errstr(error_code);
    return error_name_output;
}

std::string dsqlite::error_name(const int &error_code) {
    std::string to_return;
    return dsqlite::error_name(error_code, to_return);
}

std::string dsqlite::like_escape_at(const std::string &input) {
    static const std::regex vowel_re("@|%|_", std::regex::optimize);
    return std::regex_replace(input, vowel_re, "@$&");
}

dsqlite::SqlIn::SqlIn(uint16_t size, uint16_t offset) : size(size), offset(offset){}

std::string dsqlite::SqlIn::in_str() {
    std::string out = "IN(";
    if(this->size > 0){
        out += "?";
        out += std::to_string(this->offset);
    }
    for(uint16_t i = offset+1; i < this->offset+this->size; i++){
        out += ", ?";
        out += std::to_string(i);
    }
    out += ")";
    return out;
}
