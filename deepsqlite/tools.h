//
// Created by Dominik Przybysz.
//

#ifndef DEEPSQLITE_TOOLS_H
#define DEEPSQLITE_TOOLS_H

#include "types.h"
#include "query.h"

namespace dsqlite{
    /// Make input std::string sqlite safe
    std::string escape(const std::string & input);

    /// \param error_code - code of sqlite error which will be described
    /// \param error_name_output - std::string which will contain returned message/name
    /// \return - name or short description of error with passed code
    std::string & error_name(const sqlite_code_t & error_code, std::string & error_name_output);
    /// \param error_code - code of sqlite error which will be described
    /// \return - name or short description of error with passed code
    std::string error_name(const int & error_code);

    /// \param input - phase for sql LIKE parameter
    /// \return - input with '@' and '%' and '_' escaped via '@'
    std::string like_escape_at(const std::string & input);

    class SqlIn{
        uint16_t size;
        uint16_t offset;

    public:
        SqlIn(uint16_t size, uint16_t offset = 1);

        template<class T>
        SqlIn(const std::vector<T> & vec, uint16_t offset=1) : size(vec.size()), offset(offset){}

        std::string in_str();

        template<class T>
        StatementQuery & bind(StatementQuery & query, const std::vector<T> & vec, bool bind_null = true){
            if(vec.size()>this->size){
                throw std::exception(); // TODO
            }

            int i=0;
            for(auto const& value: vec)
                query.bind(this->offset+(i++), value);

            if(bind_null){
                for(i+this->offset; i<this->size; i++)
                    query.bind_null(i+this->offset);
            }

            return query;
        }
    };
}

#endif //DEEPSQLITE_TOOLS_H
