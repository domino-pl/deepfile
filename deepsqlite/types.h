//
// Created by Dominik Przybysz.
//

#ifndef DEEPSQLITE_TYPES_H
#define DEEPSQLITE_TYPES_H

namespace dsqlite{
    /// Number of parameter to bind
    typedef int parameter_number_t;
    /// Number of column in result record
    typedef int column_number_t;
    /// Datatype
    enum DsqliteType{
        DSQLITE_INTEGER = SQLITE_INTEGER,
        DSQLITE_FLOAT = SQLITE_FLOAT,
        DSQLITE_BLOB = SQLITE_BLOB,
        DSQLITE_NULL = SQLITE_NULL,
        DSQLITE_TEXT=SQLITE3_TEXT
    };
    /// Sqlite error/result code
    typedef int sqlite_code_t;
}

#endif //DEEPSQLITE_TYPES_H
