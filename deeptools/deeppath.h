//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_DEEPPATH_H
#define DEEPFILE_DEEPPATH_H

#include <filesystem>
#include <iostream>
namespace deep{

    class deeppath : public std::filesystem::path{
        using std::filesystem::path::path;
    protected:
        deeppath & add_sep(){
            (*this) /= "";
            return *this;
        }
    public:

        deeppath(const std::filesystem::path & path) : std::filesystem::path(path){
        }

        [[nodiscard]] deeppath copy() const{
            return *this;
        }

        static std::filesystem::path add_sep(std::filesystem::path & path){
            return path/="";
        }

        bool is_root() const {
            return (this->has_root_path() && std::filesystem::equivalent(*this, this->root_path()));
        }

        deeppath parent_path(){
            return ((*this/"").std::filesystem::path::parent_path().std::filesystem::path::parent_path()/="");
        }

        deeppath & normalize() {
            if(!this->is_absolute()){
                *this = (std::filesystem::absolute(*this));
            }

            *this = this->make_preferred().lexically_normal();

            return *this;
        }

        deeppath & normalize_dir() {
            return this->normalize().add_sep();
        }
    };
}

#endif //DEEPFILE_DEEPPATH_H
