//
// Created by Dominik Przybysz.
//

#include "exceptions.h"

std::string dexception::DeepException::details() const noexcept {
    return this->what();
}