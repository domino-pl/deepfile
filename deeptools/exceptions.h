//
// Created by Dominik Przybysz.
//

#ifndef DEEPTOOLS_EXCEPTIONS_H
#define DEEPTOOLS_EXCEPTIONS_H

#include <string>

namespace dexception{
    class DeepException : public std::exception{
    public:
        [[nodiscard]] const char * what() const noexcept override{
            return "Unspecified error";
        }
        /// Return all information about exception
        [[nodiscard]] virtual std::string details() const noexcept;
    };

    class WrongKey : public DeepException{
    public:
        [[nodiscard]] const char * what() const noexcept override {
            return "Wrong key";
        }
    };
}

#endif //DEEPFILE_EXCEPTIONS_H
