//
// Created by Dominik Przybysz.
//

#include "filesystem.h"

time_t deep::timestamp(const std::filesystem::file_time_type &time_point) {
    return std::chrono::duration_cast<std::chrono::seconds>(time_point.time_since_epoch()).count();
}

bool deep::path_contain(std::filesystem::path path1, std::filesystem::path path2) {
    if(path1 == path2)
        return true;

    std::filesystem::path * shorter;
    std::filesystem::path * longer;

    if(path1 > path2){
        longer = &path1;
        shorter = &path2;
    }else{
        shorter = &path1;
        longer = &path2;
    }

    do{
        *longer = longer->parent_path();
        if(*longer == *shorter)
            return true;
    }while(*longer > *shorter);

    return false;
}
