//
// Created by Dominik Przybysz.
//

#ifndef DEEPTOOLS_FILESYSTEM_H
#define DEEPTOOLS_FILESYSTEM_H

#include <filesystem>

namespace deep{
    /// \param time_point - file system time_point
    /// \return - unix timestamp
    time_t timestamp(const std::filesystem::file_time_type & time_point);

    /// \param path1
    /// \param path2
    /// \return true if paths contains each other
    bool path_contain(std::filesystem::path path1, std::filesystem::path path2);
}

#endif //DEEPTOOLS_FILESYSTEM_H
