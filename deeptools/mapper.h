//
// Created by Dominik Przybysz.
//

#ifndef DEEPTOOLS_MAPPER_H
#define DEEPTOOLS_MAPPER_H

#include <map>
#include "exceptions.h"

namespace deep {
    /// Class allowing map value to key like std::map but also get key based on value
    /// \tparam Key_t - key type
    /// \tparam Value_t - value type
    template<class Key_t,                                  // map::key_type
            class Value_t                                      // map::mapped_type
    >
    class Mapper : public std::map<Key_t, Value_t> {
        using std::map<Key_t, Value_t>::map;
    public:
        /// \param wanted_value
        /// \return - true if mapper contains wanted value
        bool has_value(const Value_t &wanted_value) const{
            for (std::pair<Key_t, Value_t> element : *this) {
                if (element.second == wanted_value)
                    return true;
            };
            return false;
        }

        /// \param wanted_value
        /// \return key of wanted value
        Key_t get_key(const Value_t &wanted_value) const {
            for (std::pair<Key_t, Value_t> element : *this) {
                if (element.second == wanted_value)
                    return element.first;
            };
            throw dexception::WrongKey();
        }

        /// \param k - wanted key
        /// \return - returns value by key like operator[] but this method is const
        const Value_t get(const Key_t &k) const{ // NOLINT(readability-const-return-type)
            return this->at(k);
        }
    };
}

#endif //DEEPFILE_MAPPER_H
