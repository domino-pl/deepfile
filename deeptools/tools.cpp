//
// Created by Dominik Przybysz.
//

#include "tools.h"
#include <algorithm>

using namespace deep;

std::string & deep::strLower(std::string & input) {
    std::for_each(input.begin(), input.end(), [](char & c){
        c = static_cast<char>(std::tolower(c));
    });
    return input;
}

std::string & deep::strUpper(std::string & input) {
    std::for_each(input.begin(), input.end(), [](char & c){
        c = static_cast<char>(std::toupper(c));
    });

    return input;
}

std::string deep::strBool(const bool &value) {
    return (value ? "true" : "false");
}

std::string deep::datatime(const time_t timestamp) {
    struct tm * dt;
    char buffer [30];
    dt = localtime(&timestamp);
    strftime(buffer, sizeof(buffer), "%Y.%m.%d %H:%M:%S", dt);
    return std::string(buffer);
}
