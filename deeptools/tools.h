//
// Created by Dominik Przybysz.
//

#ifndef DEEPTOOLS_TOOLS_H
#define DEEPTOOLS_TOOLS_H
#include <string>
#include <vector>
#include <ostream>

namespace deep{
    /// \param input - input string which will be modified
    /// \return modified lower case string
    std::string & strLower(std::string & input);
    /// \param input - input string which will be modified
    /// \return modified upper case string
    std::string & strUpper(std::string & input);

    /// \param input - input string which will be modified
    /// \return modified lower case copy of input string
    [[nodiscard]] inline std::string & strLowerCopy(std::string input){
        return strLower(input);
    }
    /// \param input - input string which will be modified
    /// \return modified upper case copy of input string
    [[nodiscard]] inline std::string & strUpperCopy(std::string input){
        return strUpper(input);
    }

    /// \param value - input bool value
    /// \return - std::string "true" or "false"
    std::string strBool(const bool & value);

    /// \param timestamp
    /// \return - datatime string
    std::string datatime(const time_t timestamp);
}

/// Prints all elements of std::vector to stream
/// \tparam T - auto detected vector type
/// \param stream - destination stream
/// \param vec - printed std::vector
/// \return - reference to destination stream
template < class T >
std::ostream & operator <<(std::ostream & stream, const std::vector<T> & vec){
    int i = 0;
    for(auto const& value: vec)
        stream << i++ << "): " << value << std::endl;
    return stream;
}

#endif //DEEPSQLITE_TOOLS_H
