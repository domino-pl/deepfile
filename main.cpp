#include "program/deepfile.h"

int main(int argc, char * argv[]) {
    DeePFile program;
    program.set_params(argc, argv);
    return program.run();
}