/* MD5
 converted to C++ class by Frank Thilo (thilo@unix-ag.org)
 for bzflag (http://www.bzflag.org)

   based on:

   md5.h and md5.c
   reference implemantion of RFC 1321

   Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
rights reserved.

License to copy and use this software is granted provided that it
is identified as the "RSA Data Security, Inc. MD5 Message-Digest
Algorithm" in all material mentioning or referencing this software
or this function.

License is also granted to make and use derivative works provided
that such works are identified as "derived from the RSA Data
Security, Inc. MD5 Message-Digest Algorithm" in all material
mentioning or referencing the derived work.

RSA Data Security, Inc. makes no representations concerning either
the merchantability of this software or the suitability of this
software for any particular purpose. It is provided "as is"
without express or implied warranty of any kind.

These notices must be retained in any copies of any part of this
documentation and/or software.

*/

#include <cstring>

#include "md5.h"
#include "md5errors.h"

/// Constants for MD5Transform routine.
enum {
    S11=7, 
    S12=12, 
    S13=17,
    S14=22,
    S21=5,
    S22=9, 
    S23=14, 
    S24=20, 
    S31=4, 
    S32=11, 
    S33=16, 
    S34=23, 
    S41=6, 
    S42=10, 
    S43=15,
    S44=21
};

/// F, G, H and I are basic MD5 functions.
inline MD5::uint4B MD5::F(uint4B x, uint4B y, uint4B z) {
    return (x&y) | (~x&z);
}
inline MD5::uint4B MD5::G(uint4B x, uint4B y, uint4B z) {
    return (x&z) | (y&~z);
}
inline MD5::uint4B MD5::H(uint4B x, uint4B y, uint4B z) {
    return x^y^z;
}
inline MD5::uint4B MD5::I(uint4B x, uint4B y, uint4B z) {
    return y ^ (x | ~z);
}

/// rotate_left rotates x left n bits.
inline MD5::uint4B MD5::rotate_left(uint4B x, uint4B n) {
    return (x << n) | (x >> (32-n));
}

/// FF, GG, HH, and II transformations for rounds 1, 2, 3, and 4.
/// Rotation is separate from addition to prevent recomputation.
inline void MD5::FF(uint4B &a, uint4B b, uint4B c, uint4B d, uint4B x, uint4B s, uint4B ac) {
    a = rotate_left(a+ F(b,c,d) + x + ac, s) + b;
}
inline void MD5::GG(uint4B &a, uint4B b, uint4B c, uint4B d, uint4B x, uint4B s, uint4B ac) {
    a = rotate_left(a + G(b,c,d) + x + ac, s) + b;
}
inline void MD5::HH(uint4B &a, uint4B b, uint4B c, uint4B d, uint4B x, uint4B s, uint4B ac) {
    a = rotate_left(a + H(b,c,d) + x + ac, s) + b;
}
inline void MD5::II(uint4B &a, uint4B b, uint4B c, uint4B d, uint4B x, uint4B s, uint4B ac) {
    a = rotate_left(a + I(b,c,d) + x + ac, s) + b;
}


void MD5::init()
{
    finalized=false;
    bytes_counter = 0;

    // load magic initialization constants.
    state[0] = 0x67452301;
    state[1] = 0xefcdab89;
    state[2] = 0x98badcfe;
    state[3] = 0x10325476;
}


/// Assumes len is a multiple of 4.
/// \param output - pointier to output uint4B
/// \param input - pointier to input unsigned char which will be decoded to uint4B
/// \param len - size in bytes data
void MD5::decode(uint4B output[], const uchar input[], size_type len)
{
    for (size_type i = 0, j = 0; j < len; i++, j += 4)
        output[i] = (static_cast<uint4B>(input[j])) | ((static_cast<uint4B>(input[j + 1])) << 8u) |
                    ((static_cast<uint4B>(input[j + 2])) << 16u) | ((static_cast<uint4B>(input[j + 3])) << 24u);
}


// encodes input (uint4B) into output (unsigned char). Assumes len is
// a multiple of 4.
/// Assumes len is a multiple of 4.
/// \param output - pointier to output unsigned char
/// \param input - pointier to input uint4B which will be decoded to uint4B
/// \param len - size in bytes of data
void MD5::encode(uchar output[], const uint4B input[], size_type len)
{
    for (size_type i = 0, j = 0; j < len; i++, j += 4) {
        output[j] = input[i] & 0xffu;
        output[j+1] = (input[i] >> 8u) & 0xffu;
        output[j+2] = (input[i] >> 16u) & 0xffu;
        output[j+3] = (input[i] >> 24u) & 0xffu;
    }
}

/// apply MD5 algo on a block
/// \param block - decoded block of input data
void MD5::transform(const uchar block[MD5_BLOCKSIZE])
{
    uint4B a = state[0], b = state[1], c = state[2], d = state[3], x[16];
    decode (x, block, MD5_BLOCKSIZE);

    /* Round 1 */
    FF (a, b, c, d, x[ 0], S11, 0xd76aa478); /* 1 */
    FF (d, a, b, c, x[ 1], S12, 0xe8c7b756); /* 2 */
    FF (c, d, a, b, x[ 2], S13, 0x242070db); /* 3 */
    FF (b, c, d, a, x[ 3], S14, 0xc1bdceee); /* 4 */
    FF (a, b, c, d, x[ 4], S11, 0xf57c0faf); /* 5 */
    FF (d, a, b, c, x[ 5], S12, 0x4787c62a); /* 6 */
    FF (c, d, a, b, x[ 6], S13, 0xa8304613); /* 7 */
    FF (b, c, d, a, x[ 7], S14, 0xfd469501); /* 8 */
    FF (a, b, c, d, x[ 8], S11, 0x698098d8); /* 9 */
    FF (d, a, b, c, x[ 9], S12, 0x8b44f7af); /* 10 */
    FF (c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
    FF (b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
    FF (a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
    FF (d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
    FF (c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
    FF (b, c, d, a, x[15], S14, 0x49b40821); /* 16 */

    /* Round 2 */
    GG (a, b, c, d, x[ 1], S21, 0xf61e2562); /* 17 */
    GG (d, a, b, c, x[ 6], S22, 0xc040b340); /* 18 */
    GG (c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
    GG (b, c, d, a, x[ 0], S24, 0xe9b6c7aa); /* 20 */
    GG (a, b, c, d, x[ 5], S21, 0xd62f105d); /* 21 */
    GG (d, a, b, c, x[10], S22,  0x2441453); /* 22 */
    GG (c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
    GG (b, c, d, a, x[ 4], S24, 0xe7d3fbc8); /* 24 */
    GG (a, b, c, d, x[ 9], S21, 0x21e1cde6); /* 25 */
    GG (d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
    GG (c, d, a, b, x[ 3], S23, 0xf4d50d87); /* 27 */
    GG (b, c, d, a, x[ 8], S24, 0x455a14ed); /* 28 */
    GG (a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
    GG (d, a, b, c, x[ 2], S22, 0xfcefa3f8); /* 30 */
    GG (c, d, a, b, x[ 7], S23, 0x676f02d9); /* 31 */
    GG (b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */

    /* Round 3 */
    HH (a, b, c, d, x[ 5], S31, 0xfffa3942); /* 33 */
    HH (d, a, b, c, x[ 8], S32, 0x8771f681); /* 34 */
    HH (c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
    HH (b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
    HH (a, b, c, d, x[ 1], S31, 0xa4beea44); /* 37 */
    HH (d, a, b, c, x[ 4], S32, 0x4bdecfa9); /* 38 */
    HH (c, d, a, b, x[ 7], S33, 0xf6bb4b60); /* 39 */
    HH (b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
    HH (a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
    HH (d, a, b, c, x[ 0], S32, 0xeaa127fa); /* 42 */
    HH (c, d, a, b, x[ 3], S33, 0xd4ef3085); /* 43 */
    HH (b, c, d, a, x[ 6], S34,  0x4881d05); /* 44 */
    HH (a, b, c, d, x[ 9], S31, 0xd9d4d039); /* 45 */
    HH (d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
    HH (c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
    HH (b, c, d, a, x[ 2], S34, 0xc4ac5665); /* 48 */

    /* Round 4 */
    II (a, b, c, d, x[ 0], S41, 0xf4292244); /* 49 */
    II (d, a, b, c, x[ 7], S42, 0x432aff97); /* 50 */
    II (c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
    II (b, c, d, a, x[ 5], S44, 0xfc93a039); /* 52 */
    II (a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
    II (d, a, b, c, x[ 3], S42, 0x8f0ccc92); /* 54 */
    II (c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
    II (b, c, d, a, x[ 1], S44, 0x85845dd1); /* 56 */
    II (a, b, c, d, x[ 8], S41, 0x6fa87e4f); /* 57 */
    II (d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
    II (c, d, a, b, x[ 6], S43, 0xa3014314); /* 59 */
    II (b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
    II (a, b, c, d, x[ 4], S41, 0xf7537e82); /* 61 */
    II (d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
    II (c, d, a, b, x[ 2], S43, 0x2ad7d2bb); /* 63 */
    II (b, c, d, a, x[ 9], S44, 0xeb86d391); /* 64 */

    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;

    // Zeroize sensitive information.
    memset(x, 0, sizeof x);
}

/// MD5 block update operation. Continues an MD5 message-digest
/// operation, processing another message block
/// \param input - input data
/// \param input_size - input data size
void MD5::update(const unsigned char input[], size_type input_size)
{
    check_not_finalized();

    /// last position in buffer
    size_type index = (bytes_counter) % MD5_BLOCKSIZE;

    /// Update number of bits
    bytes_counter += input_size;

    /// number of bytes we need to fill in buffer
    size_type firstpart = MD5_BLOCKSIZE - index;

    size_type i = 0;

    /// transform as many times as possible.
    if (input_size >= firstpart)
    {
        /// fill buffer first, transform
        memcpy(&buffer[index], input, firstpart);
        transform(buffer);

        /// transform chunks of MD5_BLOCKSIZE
        for (i = firstpart; i + MD5_BLOCKSIZE <= input_size; i += MD5_BLOCKSIZE)
            transform(&input[i]);

        index = 0;
    }

    /// buffer remaining input
    memcpy(&buffer[index], &input[i], input_size - i);
}

/// For convenience provide a verson with signed char
void MD5::update(const char input[], size_type length)
{
    update((const unsigned char*)input, length);
}
void MD5::update(const std::string & input)
{
    update(input.c_str(), input.length());
}

/// MD5 finalization. Ends an MD5 message-digest operation, writing the
/// the message digest and zeroizing the context.
MD5& MD5::finalize()
{
    check_not_finalized();

    static unsigned char padding[MD5_BLOCKSIZE] = {0x80};

    /// Save number of bits
    uchar bits_buff[8] = {0};
    uint64_t bits_counter = bytes_counter*8;
    encode(bits_buff, reinterpret_cast<const uint4B *>(&bits_counter), 8);


    /// pad out to 56 mod MD5_BLOCKSIZE.
    size_type index = bytes_counter % MD5_BLOCKSIZE;
    size_type padLen = (index < 56) ? (56 - index) : (120 - index);
    update(padding, padLen);

    /// Append length (before padding)
    update(bits_buff, 8);

    /// Store state in digest
    encode(static_cast<uchar *>(digest), state, 16);

    /// Zeroize sensitive information.
    memset(buffer, 0, sizeof buffer);
    bytes_counter = 0;
    finalized=true;

    return *this;
}

std::string MD5::hexdigest() const
{
    check_finalized();

    char buf[33];
    for (int i=0; i<16; i++)
        sprintf(buf+i*2, "%02x", digest[i]);
    buf[32]=0;

    return std::string(buf);
}

std::ostream& operator<<(std::ostream& out, const MD5 & md5)
{
    return out << md5.hexdigest();
}

std::string MD5::hash(const std::string & str)
{
    MD5 md5;
    md5.init();
    md5.update(str);
    md5.finalize();
    return md5.hexdigest();
}

MD5::MD5() {
    this->init();
}

void MD5::check_finalized() const{
    if(!this->finalized)
        throw md5error::MD5NotFinalized();
}

void MD5::check_not_finalized() const{
    if(this->finalized)
        throw md5error::MD5Finalized();
}
