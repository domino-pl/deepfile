#ifndef MD5_MD5_H
#define MD5_MD5_H
/* MD5
 converted to C++ class by Frank Thilo (thilo@unix-ag.org)
 for bzflag (http://www.bzflag.org)

   based on:

   md5.h and md5.c
   reference implementation of RFC 1321

   Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
rights reserved.

License to copy and use this software is granted provided that it
is identified as the "RSA Data Security, Inc. MD5 Message-Digest
Algorithm" in all material mentioning or referencing this software
or this function.

License is also granted to make and use derivative works provided
that such works are identified as "derived from the RSA Data
Security, Inc. MD5 Message-Digest Algorithm" in all material
mentioning or referencing the derived work.

RSA Data Security, Inc. makes no representations concerning either
the merchantability of this software or the suitability of this
software for any particular purpose. It is provided "as is"
without express or implied warranty of any kind.

These notices must be retained in any copies of any part of this
documentation and/or software.

*/

// a small class for calculating MD5 hashes of strings or byte arrays
// it is not meant to be fast or secure
//
// usage: 1) feed it blocks of uchars with update()
//      2) finalize()
//      3) get hexdigest() string
//
// assumes that char is 8 bit and int is 32 bit

#include <cstdint>
#include <string>

class MD5
{
    /// Print information about md5 sum
    friend std::ostream& operator<<(std::ostream&, const MD5 & md5);

public:
    typedef uint64_t size_type; /// data size type (bytes)

protected:
    typedef unsigned char uchar;  /// 32bit
    typedef uint32_t uint4B;  /// 32bit
    /// Size in bytes of block of data processed each time
    /// Must be multiple of 4
    static const uint16_t MD5_BLOCKSIZE=64;

protected:
    bool finalized;
    uchar buffer[MD5_BLOCKSIZE]; /// bytes that didn't fit in last MD5_BLOCKSIZE byte chunk
    size_type bytes_counter;  /// counter of processed bytes
    uint4B state[4];   /// digest so far
    uchar digest[16]; /// the result

protected:
    /// Throw md5error::MD5NotFinalized if md5 has not been finalized yet
    void check_finalized() const;
    /// Throw md5error::MD5Finalized if md5 has been finalized already
    void check_not_finalized() const ;

private:
    /// low level logic operations
    static inline uint4B F(uint4B x, uint4B y, uint4B z);
    static inline uint4B G(uint4B x, uint4B y, uint4B z);
    static inline uint4B H(uint4B x, uint4B y, uint4B z);
    static inline uint4B I(uint4B x, uint4B y, uint4B z);
    static inline uint4B rotate_left(uint4B x, uint4B n);
    static inline void FF(uint4B &a, uint4B b, uint4B c, uint4B d, uint4B x, uint4B s, uint4B ac);
    static inline void GG(uint4B &a, uint4B b, uint4B c, uint4B d, uint4B x, uint4B s, uint4B ac);
    static inline void HH(uint4B &a, uint4B b, uint4B c, uint4B d, uint4B x, uint4B s, uint4B ac);
    static inline void II(uint4B &a, uint4B b, uint4B c, uint4B d, uint4B x, uint4B s, uint4B ac);

    void transform(const uchar block[MD5_BLOCKSIZE]);
    static void decode(uint4B output[], const uchar input[], size_type len);
    static void encode(uchar output[], const uint4B input[], size_type len);

public:
    virtual void init();
    MD5();
    /// Prepare object to read hexdigest.
    /// Data can not be updated after calling this function without calling init or init_from method
    virtual MD5& finalize();
    /// Call this function after finalize method
    /// \return - Hexadecimal md5 hash of data.
    [[nodiscard]] std::string hexdigest() const;

public:
    /// These functions add data to md5 hash
    void update(const std::string & input);
    void update(const unsigned char *buf, size_type input_size);
    void update(const char *buf, size_type length);

public:
    /// \return - Number of processed data bytes till now
    [[nodiscard]] size_type read_bytes() const {
        return this->bytes_counter;
    }

public:
    /// \param str - input string
    /// \return - md5 hash of input string
    static std::string hash(const std::string & str);
};

#endif