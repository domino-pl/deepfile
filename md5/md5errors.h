//
// Created by Dominik Przybysz.
//

#ifndef MD5_MD5ERRORS_H
#define MD5_MD5ERRORS_H

#include <exception>

namespace md5error{
    class MD5Error : public std::exception{
    public:
        [[nodiscard]] const char * what() const noexcept override {
            return "MD5 creating error";
        }
    };

    class MD5FileError : public MD5Error{
    public:
        [[nodiscard]] const char * what() const noexcept override {
            return "MD5File file error";
        }
    };

    class MD5FileOpenError : public MD5FileError{
    public:
        [[nodiscard]] const char * what() const noexcept override {
            return "MD5File can not open file";
        }
    };

    class MD5FileSeekError : public MD5FileError{
    public:
        [[nodiscard]] const char * what() const noexcept override {
            return "MD5File can not seek file";
        }
    };

    class MD5FileReadingError : public MD5FileError{
    public:
        [[nodiscard]] const char * what() const noexcept override {
            return "MD5File can not read file";
        }
    };

    class MD5Finalized : public MD5Error{
    public:
        [[nodiscard]] const char * what() const noexcept override {
            return "MD5 has been already finalized";
        }
    };

    class MD5NotFinalized : public MD5Error{
    public:
        [[nodiscard]] const char * what() const noexcept override {
            return "MD5 has not been finalized yet";
        }
    };

}

#endif //MD5_MD5ERRORS_H
