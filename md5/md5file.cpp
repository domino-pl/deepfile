//
// Created by Dominik Przybysz.
//

#include "md5file.h"

MD5File & MD5File::read_file(const std::string &filepath, MD5::size_type next_size) {
    eof_flag = false;

    std::ifstream input(filepath, std::ifstream::binary);
    if(input.fail())
        throw md5error::MD5FileOpenError();

    input.seekg(this->bytes_counter);
    if(input.eof()){
        eof_flag = true;
        return *this;
    }
    if(input.fail())
        throw md5error::MD5FileSeekError();

    unsigned char data[read_block_size];
    size_type next_bytes = read_block_size;
    size_type pos = 0;

    while (!input.eof()){
        if(next_size!=0){
            pos = input.tellg();
            if(pos>=next_size)
                break;
            else if(pos+next_bytes>next_size){
                next_bytes = next_size-pos;
            }
        }

        input.read(reinterpret_cast<char *>(data), next_bytes);


        if(input.fail() && !input.eof())
            throw md5error::MD5FileReadingError();


        this->update(data, input.gcount());
    }
    eof_flag = true;
    return *this;
}

bool MD5File::eof() const {
    return eof_flag;
}
