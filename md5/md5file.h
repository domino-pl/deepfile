//
// Created by Dominik Przybysz.
//

#ifndef MD5_MD5FILE_H
#define MD5_MD5FILE_H

//#include <string>
#include "md5slice.h"
#include <fstream>
#include <iostream>
#include "md5errors.h"

class MD5File : public MD5Slice{
protected:
    /// Disallow using update methods
    using MD5Slice::update;

protected:
    /// Size of blocks reading from disk
    const uint16_t read_block_size = 4096;

protected:
    /// True if end of file was reached during last reading
    bool eof_flag = false;

public:
    /// Update md5 reading files (seek to read_bytes)
    /// \param filepath
    /// \param next_size - target position in file (0 - read to end of file), if file is shorter check read_bytes after execution
    MD5File & read_file(const std::string & filepath, size_type next_size = 0);

    /// \return - True if end of file was reached during last reading
    [[nodiscard]] bool eof() const;
};

#endif //MD5_MD5FILE_H
