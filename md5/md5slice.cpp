//
// Created by Dominik Przybysz.
//

#include "md5slice.h"

void MD5Slice::init_from(const uint64_t & old_size, const char * from){
    this->init();
    if(old_size==0)
        return;

    bytes_counter = old_size;
    state[0] = std::strtoul(from, nullptr, 16);
    state[1] = std::strtoul((const char *)from+9, nullptr, 16);
    state[2] = std::strtoul((const char *)from+18, nullptr, 16);
    state[3] = std::strtoul((const char *)from+27, nullptr, 16);

    char hex_byte[3];
    hex_byte[2]=0;
    for(size_type i=0; i<bytes_counter % MD5_BLOCKSIZE; i++){
        hex_byte[0]=from[36+i*2];
        hex_byte[1]=from[37+i*2];
        buffer[i]=(int)std::strtoul(hex_byte, nullptr, 16);
    }

}

MD5Slice& MD5Slice::finalize(const bool & create_continue_date ) {
    if(!finalized && create_continue_date){
        buffer[bytes_counter % MD5_BLOCKSIZE] = 0;
        sprintf(this->continue_data, "%08X;%08X;%08X;%08X;", state[0], state[1], state[2], state[3]);
        char *buf_ = &this->continue_data[36];
        for (size_type i = 0; i < bytes_counter % MD5_BLOCKSIZE; i++) {
            sprintf(buf_, "%02X", buffer[i]);
            buf_ += 2;
        }

    }

    MD5::finalize();

    return *this;
}

void MD5Slice::init() {
    MD5::init();
    continue_data[0] = '\0';
}

MD5Slice &MD5Slice::finalize() {
    return this->finalize(true);
}

MD5Slice::MD5Slice() {
    init();
}

MD5Slice::MD5Slice(const uint64_t &old_size, const char *from) {
    this->init_from(old_size, from);
}
