//
// Created by Dominik Przybysz.
//

#ifndef MD5_MD5SLICE_H
#define MD5_MD5SLICE_H

#include "md5.h"

class MD5Slice : public MD5{
    using MD5::MD5;

protected:
    char continue_data[36+MD5_BLOCKSIZE*2+1]; /// continue data

public:
    MD5Slice ();
    void init() override;
    /// Init MD5 object to in order to resume to calculating md5
    /// \param old_size - size of old data in bytes
    /// \param from - continue string
    void init_from(const uint64_t & old_size, const char * from);
    MD5Slice(const uint64_t & old_size, const char * from);
    /// Like overided but sets create_continue_date = true

    MD5Slice& finalize() override;
    /// \param create_continue_date - if true continue string will be prepared
    MD5Slice& finalize(const bool & create_continue_date);

public:
    /// \return continue string
    [[nodiscard]] std::string get_continue() const{
        return this->continue_data;
    }
};

#endif //MD5_MD5SLICE_H
