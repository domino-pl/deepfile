//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_ACTIONS_H
#define DEEPFILE_ACTIONS_H

#include <cinttypes>
#include <vector>

#include "../deeptools/deeppath.h"

typedef uint8_t ActionType_t;

struct ActionTypes{
public:
    static const ActionType_t HELP = 0;
    static const ActionType_t INIT_DB = 1;
    static const ActionType_t REMOVE_DB = 2;
    static const ActionType_t SCAN = 3;
    static const ActionType_t SEARCH = 4;
    static const ActionType_t DISPLAY = 5;
    static const ActionType_t CLOSE_DB = 6;
    static const ActionType_t CLEAR_UNREADABLE = 7;
    static const ActionType_t VACUUM = 8;
};

struct Action{
    ActionType_t type;
    std::vector<deep::deeppath> dirs;

    Action(ActionType_t type){
        this->type = type;
    }
};

#endif //DEEPFILE_ACTIONS_H
