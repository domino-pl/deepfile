//
// Created by Dominik Przybysz.
//

#include "../deepfile.h"
#include "../../deepsqlite/query.h"
#include "../../deepsqlite/tools.h"

void DeePFile::action_clear_unreadable(const std::vector<deep::deeppath> &paths) {
    std::cout << "Processing...";
    const char * sql_c = "";
    dsqlite::StatementQuery query;

    std::string sql_str;
    sql_str = "update files set readable=1 where dir in(select dirs_outin.din from dirs_outin join dirs on dirs.id=dirs_outin.dout where dirs.path ";
    dsqlite::SqlIn sql_in(paths);
    sql_str += sql_in.in_str();
    sql_str += ") and readable != 1";
    query.prepare(db, sql_str);
    sql_in.bind(query, params.paths);
    query.exec();

    std::cout << " done" << std::endl;
}