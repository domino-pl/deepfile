//
// Created by Dominik Przybysz.
//

#include "../deepfile.h"
#include "../../deepsqlite/query.h"
#include "../../deepsqlite/tools.h"

void DeePFile::action_display(const std::vector<deep::deeppath> &paths) {
    dsqlite::StatementQuery query;
    std::string sql_str;

    sql_str = "select dup_id, file_id, file_path,  file_size,times from v_duplicates where file_dir in (select dirs_outin.din from dirs_outin join dirs on dirs.id=dirs_outin.dout where dirs.path ";
    dsqlite::SqlIn sql_in(paths);
    sql_str += sql_in.in_str();
    sql_str += ")";
    query.prepare(db, sql_str);
    sql_in.bind(query, params.paths);
    dsqlite::QueryResult result = query.exec_read();

    uint32_t last_dup_id = -1;
    while (result.row()){
        if(last_dup_id != result.get_int32(0)){
            std::cout << "------ " << result.get_int32(4) << "*" << result.get_int32(3) << " bytes ------" << std::endl;
            last_dup_id = result.get_int32(0);
        }
        std::cout << result.get_string(2) << std::endl;
        result.next();
    }
    std::cout << "------------" << std::endl;

}
