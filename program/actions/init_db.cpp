//
// Created by Dominik Przybysz.
//

#include "../../deepfile_info.h"
#include "../deepfile.h"
#include "../../deepsqlite/query.h"

void DeePFile::action_init_db(){
    std::cout << "Create new database '" << params.db_path << "'... ";
    open_database(true);
    std::cout << "done" << std::endl;

    std::cout << "Init database... ";

    /// create_compressed.sql
    const char * sql_c = "CREATE TABLE dirs(id INTEGER PRIMARY KEY AUTOINCREMENT,scan INTEGER NOT NULL,path TEXT NOT NULL,name TEXT NOT NULL,dir INTEGER,hardlinks INTEGER);CREATE TABLE dirs_outin (dout INTEGER,din INTEGER,PRIMARY KEY (dout,din)) WITHOUT ROWID;CREATE TABLE dups (id INTEGER PRIMARY KEY,size INTEGER,sum TEXT,times INTEGER NOT NULL);CREATE TABLE files (id INTEGER PRIMARY KEY AUTOINCREMENT,scan INTEGER NOT NULL,name TEXT NOT NULL,dir INTEGER NOT NULL,mtime DATETIME,size INTEGER,hardlinks INTEGER,readable BOOLEAN DEFAULT (1),dup_status INTEGER DEFAULT NULL,dup_id INTEGER);CREATE TABLE filesystem_errors (id INTEGER PRIMARY KEY AUTOINCREMENT,fs_type,fs_id,error_id,time);CREATE TABLE filesystem_errors_dict (id INTEGER PRIMARY KEY,msg TEXT);CREATE TABLE info(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,value TEXT);CREATE TABLE md5 (file INTEGER NOT NULL,size INTEGER NOT NULL,sum TEXT NOT NULL,continiue TEXT,PRIMARY KEY (file,size));CREATE TABLE scans(id INTEGER PRIMARY KEY AUTOINCREMENT,path TEXT NOT NULL,time DATETIME NOT NULL);CREATE INDEX index_Dirs_Dir ON dirs(dir);CREATE INDEX index_Dirs_Name ON dirs(name);CREATE INDEX index_Dirs_outin_In ON dirs_outin(din);CREATE INDEX index_Dirs_outin_Out ON dirs_outin (dout);CREATE INDEX index_Dirs_Path ON dirs(path);CREATE INDEX index_Dirs_Scan ON dirs(scan);CREATE UNIQUE INDEX index_Dups_Size_Sum ON dups (sum,size);CREATE INDEX index_Files_Dir ON files (dir);CREATE INDEX index_Files_Dup_id ON files (dup_id);CREATE INDEX index_Files_Dup_status ON files (dup_status);CREATE INDEX index_Files_Name ON files (name);CREATE INDEX index_Files_Readable ON files (readable);CREATE INDEX index_Files_Scan ON files (scan);CREATE UNIQUE INDEX index_Info_Name ON info(name);CREATE INDEX index_Md5_Sum ON md5 (sum);CREATE VIEW v_files_sum AS select files.*,md5.sum from files join md5 on files.id=md5.file and md5.size>=files.size;CREATE VIEW v_filesp AS SELECT dirs.path||files.name path,files.* FROM files JOIN dirs ON dirs.id=files.dir;CREATE TABLE scan_errors (id INTEGER PRIMARY KEY AUTOINCREMENT,scan INTEGER,dir INTEGER,code INTEGER,what TEXT,path TEXT,time DATETIME);CREATE TABLE md5_errors(id   INTEGER  PRIMARY KEY AUTOINCREMENT,file INTEGER,what TEXT,time DATETIME);CREATE VIEW v_duplicates AS SELECT f.dup_id dup_id,f.id file_id,f.path file_path,f.size file_size,d.times times,f.dir file_dir FROM v_filesp f JOIN dups d ON f.dup_id=d.id WHERE f.dup_status=1 ORDER BY file_size,times,dup_id;";
    db.exec(sql_c);

    sql_c = "INSERT INTO info(name, value) VALUES(?1, ?2);";
    dsqlite::StatementQuery insert_info_query(db, sql_c);

    insert_info_query.bind(1, "app_version");
    insert_info_query.bind(2, DEEPFILE_APP_VER);
    insert_info_query.exec();

    insert_info_query.bind(1, "db_version");
    insert_info_query.bind(2, DEEPFILE_DB_VER);
    insert_info_query.exec();

    insert_info_query.bind(1, "app_build_date");
    insert_info_query.bind(2, DEEPFILE_BUILD_DATE);
    insert_info_query.exec();

    insert_info_query.bind(1, "app_build_time");
    insert_info_query.bind(2, DEEPFILE_BUILD_TIME);
    insert_info_query.exec();

    insert_info_query.bind(1, "initialized");
    insert_info_query.bind(2, 1);
    insert_info_query.exec();

    std::cout << "done" << std::endl;

};