//
// Created by Dominik Przybysz.
//

#include "../deepfile.h"

void DeePFile::action_remove_db(){
    std::cout << "Remove old database '" << params.db_path << "'... ";
    if (!std::filesystem::remove(params.db_path)) {
        std::cerr << "Error occured during removing old database file" << std::endl;
        program_exit();
    } else
    std::cout << "done" << std::endl;
};