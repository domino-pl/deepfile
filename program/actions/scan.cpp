//
// Created by Dominik Przybysz.
//

#include "../deepfile.h"
#include "../../deepsqlite/query.h"
#include "../types.h"
#include "../../deepsqlite/tools.h"
#include "scan.h"

void DeePFile::action_scan(const std::filesystem::path & start_dir){
    bool transaction = true;

    std::cout << "Scan " << start_dir << "..." << std::endl;

    std::vector <scan_id_t> covered_scans;
    scan_id_t updated_scan = 0;

    const char *sql_c = "SELECT id, path, time FROM scans WHERE path LIKE(?1||'%') ESCAPE '@' GROUP BY path HAVING max(time)";
    dsqlite::StatementQuery query(db, sql_c);
    query.bind(1, dsqlite::like_escape_at(start_dir/""));
    dsqlite::QueryResult result = query.exec_read();
    while (result.row()){
        covered_scans.push_back(result.get_int32(0));
        std::cout << "Old scan '" << result.get_string(1) << "'[" << deep::datatime(result.get_int64(2)) << "] will be covered" << std::endl;
        result.next();
    }
    result.finish();

    std::vector <deep::deeppath> parents;
    deep::deeppath parent = start_dir;
    parents.push_back(parent);
    while (!parent.is_root()){
        parent = parent.parent_path();
        parents.push_back(parent);
    }

    dsqlite::SqlIn parents_in(parents);
    std::string sql_str = "";
    sql_str += "SELECT id, path, time FROM scans WHERE path ";
    sql_str += parents_in.in_str();

    sql_str += " ORDER BY time DESC LIMIT 1";
    query.prepare(db, sql_str);
    parents_in.bind(query, parents);
    result = query.exec_read();
    if(result.row()){
        std::cout << "Old scan '" << result.get_string(1)  << "'[" << deep::datatime(result.get_int64(2)) << " will be updated" << std::endl;
        updated_scan = result.get_int32(0);
    }
    result.finish();

    db.set_synchronous(dsqlite::Database::synchronous_level::SYNC_OFF);
    db.exec("PRAGMA temp_store=memory");
    if(transaction) {
        std::cout << "Transaction begin... ";
        db.transaction_begin();
        std::cout << "done" << std::endl;
    }

    std::cout << "Scan:" <<  std::endl;
    query.prepare(db, "INSERT INTO scans(path, time) VALUES(?1, strftime('%s','now'))");
    query.bind(1, start_dir);
    query.exec();

    uint16_t scan_id = db.last_inserted_row_id();

    ScanInserter inserter(db, scan_id);
    Scanner scanner(db, scan_id);

    scanner.scan(start_dir);

    std::cout << "Post scan processing...";

    db.exec("UPDATE dirs SET dir = NULL WHERE dir=0");
    std::cout << "."; std::cout.flush();

    if(updated_scan) {
        /// ?1 - updated scan
        /// ?2 - new scan
        sql_c = "update dirs set dir=(select dirs.dir from dirs join scans on scans.path=dirs.path where scans.id=?2 and dirs.scan=?1) where id=(select dirs.id from dirs join scans on dirs.scan=scans.id where scan=?2 and scans.path=dirs.path);";
        query.prepare(db, sql_c);
        query.bind(1, updated_scan);
        query.bind(2, scan_id);
        query.exec();
        std::cout << "."; std::cout.flush();

        sql_c = "update files set scan = 0 where dir in(select din from dirs join scans on scans.path=dirs.path join dirs_outin on dirs_outin.dout=dirs.id where scans.id=?2 and dirs.scan=?1);";
        query.prepare(db, sql_c);
        query.bind(1, updated_scan);
        query.bind(2, scan_id);
        query.exec();
        std::cout << "."; std::cout.flush();

        sql_c = "update dirs set scan = 0 where id in(select din from dirs join scans on scans.path=dirs.path join dirs_outin on dirs_outin.dout=dirs.id where scans.id=?2 and dirs.scan=?1);";
        query.prepare(db, sql_c);
        query.bind(1, updated_scan);
        query.bind(2, scan_id);
        query.exec();
        std::cout << "."; std::cout.flush();
    }

    if(!covered_scans.empty()){
        dsqlite::SqlIn sql_in(covered_scans);
        sql_str = "update files set scan = 0 where scan ";
        sql_str += sql_in.in_str();
        query.prepare(db, sql_str);
        sql_in.bind(query, covered_scans);
        query.exec();
        std::cout << "."; std::cout.flush();

        sql_str = "update dirs set scan = 0 where scan ";
        sql_str += sql_in.in_str();
        query.prepare(db, sql_str);
        sql_in.bind(query, covered_scans);
        query.exec();
        std::cout << "."; std::cout.flush();

        sql_str = "delete from scan_errors where scan ";
        sql_str += sql_in.in_str();
        query.prepare(db, sql_str);
        sql_in.bind(query, covered_scans);
        query.exec();
        std::cout << "."; std::cout.flush();
    }

    if(updated_scan || !covered_scans.empty()){
        sql_c = "DROP TABLE IF EXISTS old_dirs;CREATE TEMP TABLE old_dirs (new INTEGER NOT NULL,old INTEGER);DROP INDEX IF EXISTS index_Old_dirs_New;CREATE INDEX index_Old_dirs_New ON old_dirs (new);DROP INDEX IF EXISTS index_Old_dirs_Old;CREATE INDEX index_Old_dirs_Old ON old_dirs (old);DROP TABLE IF EXISTS old_files;CREATE TEMP TABLE old_files (pair_id INTEGER PRIMARY KEY AUTOINCREMENT,new INTEGER NOT NULL,old INTEGER NOT NULL);DROP INDEX IF EXISTS index_Replaced_files_New;CREATE INDEX index_Replaced_files_New ON old_files (new);DROP INDEX IF EXISTS index_Replaced_files_Old;CREATE INDEX index_Replaced_files_Old ON old_files (old)";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        //sql_c = "insert or ignore into old_dirs select new.id, (select old.id from dirs old where old.scan=0 and old.path=new.path) old_id from dirs new where new.scan=?1";
        sql_c = "insert or ignore into old_dirs select new.id, null from dirs new where new.scan=?1";
        query.prepare(db, sql_c);
        query.bind(1, scan_id);
        query.exec();
        std::cout << "."; std::cout.flush();

        sql_c = "update old_dirs set old=(select old.id from dirs old join dirs new on old.scan=0 and new.id=old_dirs.new and new.path=old.path);";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "delete from old_dirs where old is null;";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "insert into old_files select NULL, new.id, old.id from files new join old_dirs r on new.dir = r.new join files old on old.dir=r.old and old.mtime = new.mtime and old.size=new.size;";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "delete from old_files where pair_id in (select r.pair_id from old_files r join files new on new.id=r.new join files old on old.id=r.old where new.name!=old.name);";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "update files set scan = ?1, readable=1, dir=(select new from old_dirs where old_dirs.old=files.dir) where id in(select old from old_files);";
        query.prepare(db, sql_c);
        query.bind(1, scan_id);
        query.exec();
        std::cout << "."; std::cout.flush();

        sql_c = "delete from files where id in (select new from old_files);";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "delete from md5 where file in(select id from files where scan=0);";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "delete from files where scan=0;";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "delete from dirs where scan=0;";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "delete from dirs_outin where dout in(select old from old_dirs);";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "delete from dirs_outin where din in(select old from old_dirs);";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "DROP TABLE old_dirs;";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();

        sql_c = "DROP TABLE old_files;";
        db.exec(sql_c);
        std::cout << "."; std::cout.flush();
    }

    sql_c = "insert or ignore into dirs_outin select a.id, a.id from dirs a;";
    db.exec(sql_c);
    std::cout << "."; std::cout.flush();

    sql_c = "insert or ignore into dirs_outin select a.id, b.id from dirs a, dirs b where b.dir in(select din from dirs_outin where dout=a.id);";
    query.prepare(db, sql_c);
    do{
        query.exec();
        std::cout << "."; std::cout.flush();
    }while(db.changed_rows());

    std::cout << " done" << std::endl;

    if(transaction){
        std::cout << "Transaction commit... ";
        db.transaction_commit();
        std::cout << "done" << std::endl;
    }
    db.set_synchronous(dsqlite::Database::synchronous_level::SYNC_FULL);
    db.set_synchronous(dsqlite::Database::synchronous_level::SYNC_NORMAL);
}


void ScanInserter::dir(const std::filesystem::directory_entry &entry, const uint32_t &parent_dir) {
    this->statement_dirs.bind(1, this->scan_id);
    this->statement_dirs.bind(2, entry.path()/"");
    this->statement_dirs.bind(3, (entry.path()/"").parent_path().filename());
    this->statement_dirs.bind(4, parent_dir);
    this->statement_dirs.bind(5, entry.hard_link_count());
    this->statement_dirs.exec();
}

ScanInserter::ScanInserter(dsqlite::Database &db, const uint16_t &scan_id) : scan_id(scan_id){
    this->statement_dirs.prepare(db, sql_dirs);
    this->statement_files.prepare(db, sql_files);
    this->statement_errors.prepare(db, sql_scan_error);
}

void ScanInserter::file(const std::filesystem::directory_entry &entry, const uint32_t &parent_dir) {
    this->statement_files.bind(1, this->scan_id);
    this->statement_files.bind(2, entry.path().filename());
    this->statement_files.bind(3, parent_dir);
    this->statement_files.bind(4, deep::timestamp(entry.last_write_time()));
    this->statement_files.bind(5, entry.file_size());
    this->statement_files.bind(6, entry.hard_link_count());
    this->statement_files.exec();
}

void ScanInserter::error(const std::filesystem::filesystem_error &Ex, const uint32_t &parent_dir) {
    this->statement_errors.bind(1, this->scan_id);
    this->statement_errors.bind(2, parent_dir);
    this->statement_errors.bind(3, Ex.code().value());
    this->statement_errors.bind(4, Ex.what());
    this->statement_errors.bind(5, Ex.path1());
    this->statement_errors.exec();
}

/*
void ScanInserter::error(const std::exception &Ex, const uint32_t &parent_dir) {
    this->statement_errors.bind(1, this->scan_id);
    this->statement_errors.bind(2, parent_dir);
    this->statement_errors.bind(4, Ex.what());
}
*/

void Scanner::scan_dir(std::filesystem::path path, const uint32_t &parent_dir_id) {
    inserter.dir(std::filesystem::directory_entry(path), parent_dir_id);
    uint32_t dir_id = this->db.last_inserted_row_id();
    try {
        std::filesystem::directory_iterator it(path);

        uint16_t files = 0;
        uint16_t since_last_dir = 0;
        for(auto& p: it) {
            try {
                if (p.is_symlink())
                    continue;

                if (p.is_directory()) {
                    std::cout << this->out_prefix << "DIR: " << p.path() << std::endl;
                    this->scan_dir(p.path(), dir_id);
                    since_last_dir = 0;
                } else {
                    ++since_last_dir;
                    if (p.is_regular_file()) {
                        if (since_last_dir % 1000 == 0)
                            std::cout << this->out_prefix << "FILE: " << p.path() << std::endl;
                        inserter.file(p, dir_id);
                    }
                }
            }catch (std::filesystem::filesystem_error & Ex){
                inserter.error(Ex, parent_dir_id);
                std::cerr << Ex.path1() << " " << Ex.what() << " [Code " << Ex.code() << "]";
            }
        }
    }catch (std::filesystem::filesystem_error & Ex){
        if(std::filesystem::equivalent(Ex.path1(), path)){
            inserter.error(Ex, parent_dir_id);
            std::cerr << Ex.path1() << " " << Ex.what() << " [Code " << Ex.code() << "]" << std::endl;
        }else{
            throw Ex;
        }
    }




}

Scanner::Scanner(dsqlite::Database &db, const uint16_t &scan_id, const std::string &out_prefix) : db(db), inserter(db, scan_id), out_prefix(out_prefix){

}

void Scanner::scan(std::filesystem::path start_dir) {
    this->scan_dir(start_dir, 0);
}
