//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_SCAN_H
#define DEEPFILE_SCAN_H

#include <cinttypes>
#include <filesystem>

#include "../../deepsqlite/query.h"
#include "../../deeptools/filesystem.h"

class ScanInserter{
protected:
    uint16_t scan_id;

    const char * sql_dirs = "INSERT INTO dirs(scan, path, name, dir, hardlinks) VALUES(?1, ?2, ?3, ?4, ?5)";
    const char * sql_files = "INSERT INTO files(scan, name, dir, mtime, size, hardlinks) VALUES(?1, ?2, ?3, ?4, ?5, ?6)";
    const char * sql_scan_error = "INSERT INTO scan_errors(scan, dir, code, what, path, time) VALUES(?1, ?2, ?3, ?4, ?5, strftime('%s','now'))";

    dsqlite::StatementQuery statement_dirs;
    dsqlite::StatementQuery statement_files;
    dsqlite::StatementQuery statement_errors;
public:
    ScanInserter() = delete;
    ScanInserter(dsqlite::Database & db, const uint16_t & scan_id);

    void dir(const std::filesystem::directory_entry & entry, const uint32_t & parent_dir);
    void file(const std::filesystem::directory_entry & entry, const uint32_t & parent_dir);
    void error(const std::filesystem::filesystem_error & Ex, const uint32_t &parent_dir);
    //void error(const std::exception & Ex, const uint32_t &parent_dir);
};


class Scanner{
protected:
    ScanInserter inserter;
    dsqlite::Database & db;

    std::string out_prefix;
protected:
    void scan_dir(std::filesystem::path path, const uint32_t & parent_dir_id);
public:
    Scanner() = delete;
    Scanner(dsqlite::Database & db, const uint16_t & scan_id, const std::string & out_prefix = "");
    void scan(std::filesystem::path start_dir);
};


#endif //DEEPFILE_SCAN_H
