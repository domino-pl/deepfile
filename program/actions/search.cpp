//
// Created by Dominik Przybysz.
//

#include "../deepfile.h"
#include "../../deepsqlite/query.h"
#include "../../deepsqlite/tools.h"
#include "../types.h"
#include "../../md5/md5file.h"

void set_file_md5_error(dsqlite::Database & db, file_id_t file, const char * what){
    static const char * sql_c_set_not_readable = "update files set readable=0 where id=?1";
    static const char * sql_c_insert_error = "insert into md5_errors(file, what, time) values(?1, ?2, strftime('%s','now'))";
    static dsqlite::StatementQuery set_not_readable_query(db, sql_c_set_not_readable);
    static dsqlite::StatementQuery insert_error_query(db, sql_c_insert_error);

    set_not_readable_query.bind(1, file);
    set_not_readable_query.exec();
    insert_error_query.bind(1, file);
    insert_error_query.bind(2, what);
    insert_error_query.exec();

    std::cerr << std::endl << "ERROR: " << what << std::endl;
}

void mark_unreadable_not_duplicated(dsqlite::Database & db){
    std::cout << "Mark unreadable not duplicated...";
    const char * sql_unreadable_not_duplicated = "update files set dup_status = -1 where dup_status=0 and readable = 0;";
    db.exec(sql_unreadable_not_duplicated);
    std::cout << " done" << std::endl;
}

uint32_t possible_duplicates_number(dsqlite::Database & db){
    std::cout << "Getting number of possible duplicates: ";
    static const char * sql_c =  "select count(*) from files where dup_status=0;";
    static dsqlite::StatementQuery query_count_possible_dups(db, sql_c);
    dsqlite::QueryResult result = query_count_possible_dups.exec_read();
    std::cout << result.get_int32(0) << std::endl;
    return result.get_int32(0);
}

class Md5CollectError : public std::exception{
public:
    [[nodiscard]] const char * what() const noexcept override{
        return "Getting md5 error";
    }
};

class NotWholeFileRead : public Md5CollectError{
public:
    [[nodiscard]] const char * what() const noexcept override{
        return "Not whole file read";
    }
};
class SizeChangedError : public Md5CollectError{
public:
    [[nodiscard]] const char * what() const noexcept override{
        return "File sized changed";
    }
};

void transaction_begin(dsqlite::Database & db, const bool use_transaction) {
    if (use_transaction) {
        std::cout << "Transaction begin... ";
        db.transaction_begin();
        std::cout << "done" << std::endl;
    }
}
void transaction_commit(dsqlite::Database & db, const bool use_transaction) {
    if (use_transaction) {
        std::cout << "Transaction commit... ";
        db.transaction_commit();
        std::cout << "done" << std::endl;
    }
}

file_size_t get_next_read_size(file_size_t last_size){
    if(last_size==0)
        return 4096;
    else if(last_size < 4096*10)
        return 4096*10;
    else if(last_size < 1024*100)
        return 1024*100;
    else if(last_size < 1024*1000)
        return 1024*1000;
    else if(last_size < 1024*10000)
        return 1024*10000;
    else if(last_size < 1024*100000)
        return 1024*100000;
    else if(last_size < 1024*1000000)
        return last_size+1024*100000;
    else
        return last_size+1024*1000000;
}

void DeePFile::action_search(const std::vector<deep::deeppath> & paths){
    bool use_transaction = true;
    db.set_synchronous(dsqlite::Database::synchronous_level::SYNC_OFF);
    db.exec("PRAGMA temp_store=memory");

    dsqlite::StatementQuery query;
    dsqlite::QueryResult result;
    const char * sql_c;
    std::string sql_str;

    transaction_begin(db, use_transaction);

    std::cout << "Preparing to search...";
    db.exec("update files set dup_status = NULL where dup_status IS NOT NULL;");
    std::cout << "."; std::cout.flush();
    db.exec("update files set dup_id = NULL where dup_id IS NOT NULL;");
    std::cout << "."; std::cout.flush();
    db.exec("delete from dups;");
    std::cout << "."; std::cout.flush();

    dsqlite::SqlIn sql_in(paths);
    sql_str = "update files set dup_status=0 where dir in(SELECT dirs_outin.din FROM dirs join dirs_outin on dirs.id=dirs_outin.dout WHERE dirs.path ";
    sql_str += sql_in.in_str();
    sql_str += ")";
    query.prepare(db, sql_str);
    sql_in.bind(query, paths);
    query.exec();
    std::cout << ".";
    std::cout << " done" << std::endl;

    mark_unreadable_not_duplicated(db);

    std::cout << "Unselect size 0 files...";
    db.exec("update files set dup_status = -2 where dup_status=0 and size=0;");
    std::cout << " done" << std::endl;

    std::cout << "Unselect not duplicated file sizes...";
    db.exec("update files set dup_status = -3 where dup_status=0 and size not in(select size from files where dup_status = 0 group by size having count(size) > 1);");
    std::cout << " done" << std::endl;

    transaction_commit(db, use_transaction);

    sql_c = "select files.id, files.path, files.size from v_filesp files left join md5 on files.id=md5.file and md5.size=?1 where files.dup_status=0 and md5.size is null";
    dsqlite::StatementQuery query_no_md5(db, sql_c);
    sql_c = "select files.id, files.path, files.size, md5.size as md5_size, md5.sum, md5.continiue from v_filesp files join md5 on files.id=md5.file and md5.size<?1 left join md5 md5_current on md5_current.file=files.id and md5_current.size=?1 where files.dup_status=0 and md5_current.sum is null group by files.id having max(md5.size)";
    dsqlite::StatementQuery query_continue_md5(db, sql_c);
    sql_c = "INSERT INTO md5 (file, size, sum, continiue) VALUES(?1, ?2, ?3, ?4)";
    dsqlite::StatementQuery query_insert_md5(db, sql_c);
    sql_c = "update files set dup_status=-5 where id in (select id from files join md5 on files.id=md5.file where dup_status=0 and md5.size=?1 group by files.size, md5.sum having count(*)=1)";
    dsqlite::StatementQuery query_unselect_not_duplicated(db, sql_c);
    sql_c = "update files set dup_status=1 where dup_status=0 and size<=?1";
    dsqlite::StatementQuery query_mark_duplicates(db, sql_c);

    dsqlite::StatementQuery * query_md5;
    dsqlite::QueryResult query_md5_result;

    MD5File md5;
    file_size_t next_size = 0;
    file_id_t file_id;
    file_size_t file_size;
    deep::deeppath file_path;
    file_size_t last_md5_size = 0;
    std::string last_continue_md5;
    std::string last_md5;
    bool whole_file_read;
    bool first = true;

    while(possible_duplicates_number(db)){
        next_size = get_next_read_size(next_size);

        std::cout << "Calculating " << next_size << " bytes md5 sum:" << std::endl;

        if(first)
            query_md5 = &query_no_md5;
        else
            query_md5 = &query_continue_md5;

        query_md5->bind(1, next_size);
        query_md5_result = query_md5->exec_read();

        while (query_md5_result.row()) {
            file_id = query_md5_result.get_int32(0);
            file_path = query_md5_result.get_string(1);
            try {
                file_size = query_md5_result.get_int64(2);
                if (!first) {
                    last_continue_md5 = query_md5_result.get_string(5);
                    last_md5_size = query_md5_result.get_int64(3);
                }
                whole_file_read = next_size >= file_size;

                std::cout << file_path << "... ";
                std::cout.flush();

                try {
                    if (std::filesystem::file_size(file_path) != file_size)
                        throw SizeChangedError();

                    if (first)
                        md5.init();
                    else
                        md5.init_from(last_md5_size, last_continue_md5.c_str());

                    md5.read_file(file_path, next_size).finalize(!whole_file_read);
                    std::cout << md5 << std::endl;

                    // TODO
                    /*if(whole_file_read && file_size!=md5.read_bytes()) {
                        std::cerr << file_path << ";" << file_size << ";" << md5.read_bytes();
                        throw NotWholeFileRead();
                    }*/

                    query_insert_md5.bind(1, file_id);
                    query_insert_md5.bind(2, next_size);
                    query_insert_md5.bind(3, md5.hexdigest());
                    if (!whole_file_read)
                        query_insert_md5.bind(4, md5.get_continue());
                    query_insert_md5.exec();


                } catch (const std::filesystem::__cxx11::filesystem_error &Ex) {
                    set_file_md5_error(db, file_id, Ex.what());
                } catch (const Md5CollectError &Ex) {
                    set_file_md5_error(db, file_id, Ex.what());
                } catch (const md5error::MD5Error &Ex) {
                    set_file_md5_error(db, file_id, Ex.what());
                }
            }catch(const std::exception & Ex){
                std::cerr << std::endl << "Unknown error for file " << file_path << "[" << file_id << "]" << std::endl;
                throw Ex;
            }
            query_md5_result.next();
        }

        first = false;
        query_md5_result.finish();

        transaction_begin(db, use_transaction);

        mark_unreadable_not_duplicated(db);

        std::cout << "Unselect not duplicated files...";
        query_unselect_not_duplicated.bind(1, next_size);
        query_unselect_not_duplicated.exec();
        std::cout << " done" << std::endl;

        std::cout << "Mark duplicated files...";
        query_mark_duplicates.bind(1, next_size);
        query_mark_duplicates.exec();
        std::cout << " done" << std::endl;

        transaction_commit(db, use_transaction);
    }

    transaction_begin(db, use_transaction);

    std::cout << "Post processing...";

    sql_c = "insert into dups select null, files.size, md5.sum, count(*) from files join md5 on files.id = md5.file where dup_status=1 and files.size<=md5.size group by files.size, md5.sum having count(*)>1";
    db.exec(sql_c);
    std::cout << "."; std::cout.flush();

    sql_c = "update files set dup_id=(select dups.id from dups join md5 on dups.sum=md5.sum and md5.file=files.id and files.size=dups.size) where dup_status=1";
    db.exec(sql_c);
    std::cout << "."; std::cout.flush();
    std::cout << std::endl;

    transaction_commit(db, use_transaction);

    db.set_synchronous(dsqlite::Database::synchronous_level::SYNC_EXTRA);
    db.set_synchronous(dsqlite::Database::synchronous_level::SYNC_NORMAL);

    sql_c = "select count(*) from files where dup_status=1";
    query.prepare(db, sql_c);
    std::cout << "Found " << query.exec_read().get_int64(0) << " duplicated files." << std::endl;
}