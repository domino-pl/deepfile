//
// Created by Dominik Przybysz.
//

#include "../deepfile.h"

void DeePFile::action_vacuum() {
    std::cout << "Vacuum...";
    db.exec("vacuum");
    std::cout << " done" << std::endl;
}