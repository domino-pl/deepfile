//
// Created by Dominik Przybysz.
//

#include <cstdlib>

#include "deepfile.h"
#include "../deepsqlite/errors.h"

void DeePFile::set_params(const int &main_argc, char **main_argv) {
    argc = main_argc;
    argv = main_argv;
}

int DeePFile::run() {
    std::cout << "DeePfile!!!" << std::endl << std::endl;

    params.parse(argc, argv);
    if(params.parsing_error) {
        std::cerr << "Received wrong arguments!" << std::endl;
        params.mode = ProgramMode::HELP;
    }

    try {
        if (params.mode != ProgramMode::HELP && params.mode != ProgramMode::INIT){
            std::cout << "Open database... ";
            open_database();
            std::cout << "done" << std::endl;
        }
        switch (params.mode) {
            case ProgramMode::HELP:
                actions.push (Action(ActionTypes::HELP));
                break;
            case ProgramMode::INIT:
                mode_check_init();
                break;
            case ProgramMode::VACUUM:
                actions.push (Action(ActionTypes::VACUUM));
                break;
            case ProgramMode::CLEAR_UNREADABLE:
                mode_check_clear_unreadable();
                break;
            case ProgramMode::SCAN:
                mode_check_scan();
                break;
            case ProgramMode::SEARCH:
                mode_check_search();
                break;
            case ProgramMode::DISPLAY:
                mode_check_display();
                break;
        }

        uint8_t actions_count = actions.size();
        uint8_t actions_i = 0;
        std::string actions_counter;

        while (!actions.empty()) {
            const Action &action = actions.front();
            actions_counter = std::string("{") + std::to_string(++actions_i) + "/" + std::to_string(actions_count) + "}";

            switch (action.type) {
                case ActionTypes::HELP:
                    action_help();
                    break;
                case ActionTypes::REMOVE_DB:
                    action_remove_db();
                    break;
                case ActionTypes::INIT_DB:
                    action_init_db();
                    break;
                case ActionTypes::SCAN:
                    action_scan(action.dirs[0]);
                    break;
                case ActionTypes::SEARCH:
                    action_search(action.dirs);
                    break;
                case ActionTypes::CLOSE_DB:
                    action_close_db();
                    break;
                case ActionTypes::CLEAR_UNREADABLE:
                    action_clear_unreadable(action.dirs);
                    break;
                case ActionTypes::DISPLAY:
                    action_display(action.dirs);
                    break;
                case ActionTypes::VACUUM:
                    action_vacuum();
                    break;
            }

            actions.pop();
        }

    }catch (dsqlite::errors::SqliteError &Ex){
        std::cerr << std::endl << Ex.details() << std::endl;
    }

    std::cout << std::endl;
    return 0;
}

void DeePFile::program_exit() {
    std::cout << std::endl;
    close_database();
    std::cout << std::endl;
    std::cout.flush();
    exit(1);
}

void DeePFile::open_database(bool create) {
    try {
        db.open(params.db_path, create);
    }catch(dsqlite::errors::SqliteError & Ex){
        std::cerr << std::endl << "Error occured during opening database ('" << params.db_path << "'):" << std::endl;
        std::cerr << Ex.details() << std::endl;
        program_exit();
    }
}

void DeePFile::close_database(){
        if(!db.is_ready())
            return;

        std::cout << "Close database...";
        try {
            db.close();
        }catch(dsqlite::errors::SqliteError & Ex){
        }
        std::cout << " done" << std::endl;
};