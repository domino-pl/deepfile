//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_DEEPFILE_H
#define DEEPFILE_DEEPFILE_H

#include "../deepsqlite/database.h"
#include "params.h"
#include "actions.h"
#include "queue"

class DeePFile {
protected:
    int argc;
    char ** argv;

    dsqlite::Database db;
    ProgramParams params;
    std::queue <Action> actions;

protected:
    void open_database(bool create = false);
    void close_database();

protected:
    void mode_check_init();
    void mode_check_scan();
    void mode_check_search();
    void mode_check_clear_unreadable();
    void mode_check_display();

protected:
    void action_help();
    void action_remove_db();
    void action_init_db();
    void action_close_db();
    void action_scan(const std::filesystem::path & start_dir);
    void action_search(const std::vector<deep::deeppath> & paths);
    void action_clear_unreadable(const std::vector<deep::deeppath> & paths);
    void action_vacuum();
    void action_display(const std::vector<deep::deeppath> & paths);

public:
    void set_params(const int & main_argc, char * main_argv[]);

    void program_exit();

    int run();
};


#endif //DEEPFILE_DEEPFILE_H
