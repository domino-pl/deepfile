//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_EXIT_H
#define DEEPFILE_EXIT_H

#include <cstdlib>

inline void program_exit(int status = 1){
    exit(status);
}

#endif //DEEPFILE_EXIT_H
