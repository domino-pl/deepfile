//
// Created by Dominik Przybysz.
//

#include "deepfile.h"
#include "../deeptools/filesystem.h"
#include "../deepsqlite/query.h"
#include "../deepsqlite/tools.h"


void DeePFile::mode_check_init(){
    bool exists = std::filesystem::exists(params.db_path);

    if (exists){
        if (!std::filesystem::is_regular_file(params.db_path)){
            std::cerr << "Specified database path '" << params.db_path << "' isn't file" << std::endl;
            program_exit();
        }
    }

    if (exists){
        std::cout << "Database file already exists - ";
        if (params.force) {
            std::cout << "forcing remove old database" <<std::endl;
        } else {
            std::cout << "use force option to overwrite" << std::endl;
            program_exit();
        }
    }

    if (exists && params.force)
        actions.push (Action(ActionTypes::REMOVE_DB));

    actions.push (Action(ActionTypes::INIT_DB));
    actions.push (Action(ActionTypes::CLOSE_DB));
}

void DeePFile::mode_check_scan(){
    if(params.paths.empty()){
        std::cerr << std::endl << "Any path specified to scan" << std::endl;
        program_exit();
    }

    //std::filesystem::path litter, bigger;
    for (auto it1 = params.paths.begin(); it1 != params.paths.end(); ++it1) {
        // TODO: function
        for (auto it2 = it1 + 1; it2 != params.paths.end(); ++it2) {
            if (deep::path_contain(*it1, *it2)) {
                std::cerr << *it1 << " and " << *it1 << " contain each other" << std::endl;
                program_exit();
            }
        }

        if (!std::filesystem::exists(*it1)) {
            std::cerr << *it1 << " does not exists" << std::endl;
            program_exit();
        }

        if (!std::filesystem::is_directory(*it1)) {
            std::cerr << *it1 << " is not directory" << std::endl;
            program_exit();
        }

        actions.push(Action(ActionTypes::SCAN));
        actions.back().dirs.push_back(*it1);
    }

    actions.push (Action(ActionTypes::CLOSE_DB));
}

void DeePFile::mode_check_search(){
    if(params.paths.empty()){
        std::cerr << std::endl << "Any path specified to scan" << std::endl;
        program_exit();
    }

    dsqlite::StatementQuery query;
    dsqlite::QueryResult result;
    const char * sql_c;

    sql_c = "SELECT count(*) FROM dirs WHERE path=?1";
    query.prepare(db, sql_c);
    for (auto & path : params.paths){
        query.bind(1, path);
        result = query.exec_read();
        if(result.get_int32(0)==0){
            std::cerr << std::endl << "Dir " << path << " not in scanned dirs" << std::endl;
            program_exit();
        }
        result.finish();
    }
    query.finalize();

    actions.push(Action(ActionTypes::SEARCH));
    actions.back().dirs = params.paths;

    actions.push (Action(ActionTypes::CLOSE_DB));
}

void DeePFile::mode_check_display() {
    const char * sql_c = "";
    dsqlite::StatementQuery query;
    dsqlite::QueryResult result;

    // TODO: fuction (duplicated with mode_check_search)
    sql_c = "SELECT count(*) FROM dirs WHERE path=?1";
    query.prepare(db, sql_c);
    for (auto & path : params.paths){
        query.bind(1, path);
        result = query.exec_read();
        if(result.get_int32(0)==0){
            std::cerr << std::endl << "Dir " << path << " not in scanned dirs" << std::endl;
            program_exit();
        }
        result.finish();
    }

    std::string sql_str;
    sql_str = "select count(*) from files where dir in(select dirs_outin.din from dirs_outin join dirs on dirs.id=dirs_outin.dout where dirs.path ";
    dsqlite::SqlIn sql_in(params.paths);
    sql_str += sql_in.in_str();
    sql_str += ") and dup_status is null";
    query.prepare(db, sql_str);
    sql_in.bind(query, params.paths);
    result = query.exec_read();

    if(result.get_int64(0)>0){
        std::cerr << std::endl << "Some files in specified dirs haven't been checked yes as duplicates - use search option before" << std::endl;
        program_exit();
    }

    actions.push(Action(ActionTypes::DISPLAY));
    actions.back().dirs = params.paths;

    actions.push (Action(ActionTypes::CLOSE_DB));
}

void DeePFile::mode_check_clear_unreadable(){
    const char * sql_c = "";
    dsqlite::StatementQuery query;
    dsqlite::QueryResult result;

    // TODO: fuction (duplicated with mode_check_search)
    sql_c = "SELECT count(*) FROM dirs WHERE path=?1";
    query.prepare(db, sql_c);
    for (auto & path : params.paths){
        query.bind(1, path);
        result = query.exec_read();
        if(result.get_int32(0)==0){
            std::cerr << std::endl << "Dir " << path << " not in scanned dirs" << std::endl;
            program_exit();
        }
        result.finish();
    }

    actions.push(Action(ActionTypes::CLEAR_UNREADABLE));
    actions.back().dirs = params.paths;

    actions.push (Action(ActionTypes::CLOSE_DB));
}

