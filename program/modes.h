//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_MODES_H
#define DEEPFILE_MODES_H

#include "../deeptools/mapper.h"

enum ProgramMode{
    HELP,
    INIT,
    CLEAR_UNREADABLE,
    SCAN,
    SEARCH,
    DISPLAY,
    FORCE,
    VACUUM
};

const deep::Mapper<const ProgramMode, const std::string> programModeMapper {
        {ProgramMode::HELP,    "help"},
        {ProgramMode::INIT,    "init"},
        {ProgramMode::CLEAR_UNREADABLE,   "clear_unreadable"},
        {ProgramMode::SCAN,    "scan"},
        {ProgramMode::SEARCH,  "search"},
        {ProgramMode::DISPLAY, "display"},
        {ProgramMode::VACUUM, "vacuum"},
        {ProgramMode::FORCE,   "force"}
};

#endif //DEEPFILE_MODES_H
