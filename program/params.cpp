//
// Created by Dominik Przybysz.
//

#include <ostream>

#include "modes.h"
#include "params.h"
#include "../deeptools/tools.h"



std::ostream &operator<<(std::ostream &stream, const ProgramParams &params){
    stream << "Parsing error: " << deep::strBool(params.parsing_error) << std::endl;
    stream << "Database path: " << params.db_path << std::endl;
    stream << "Output file path: " << params.output_file << std::endl;
    stream << "Force: " << deep::strBool(params.force) << std::endl;
    stream << "Mode: " << params.mode << '('  << programModeMapper.get(params.mode) << ')' << std::endl;
    stream << "Dirs paths: " << std::endl;
    stream << params.paths;
    stream << std::endl;
    return stream;
}

void ParamsShowHelp() {
    std::cout << "Usage: DeePFile DeePfile.db [force] help/init/scan/search/display/vacuum [input_dir_1] [input_dir_2] ..." << std::endl;
}

ProgramParams &ProgramParams::parse(const int argc, char **argv, const bool throw_error) {
    uint8_t argi = 1; /// current argument number

    std::string argstr; /// current argument value converted to string
    std::string argstrl; /// current argument value converted to lowercase string
    ProgramMode argmode; /// current argument value converted to program mode
    try {
        /// Get database path
        if (argc <= argi)
            throw parsingError();
        this->db_path = argv[argi];
        argi++;

        /// Get output file if provided
        if (argc <= argi)
            throw parsingError();
        argstr = argv[argi];
        argstrl = deep::strLowerCopy(argstr);
        if (!programModeMapper.has_value(argstrl)) {
            this->output_file = argstr;
            argi++;
        }

        /// Check forcing enabled
        if (argc <= argi)
            throw parsingError();
        argstr = argv[argi];
        argstrl = deep::strLowerCopy(argstr);
        if (!programModeMapper.has_value(argstrl))
            throw parsingError();
        argmode = programModeMapper.get_key(argstrl);
        if (argmode == ProgramMode::FORCE){
            this->force = true;
            argi++;
        }

        /// Get mode
        if (argc <= argi)
            throw parsingError();
        argstr = argv[argi];
        argstrl = deep::strLowerCopy(argstr);
        if (!programModeMapper.has_value(argstrl))
            throw parsingError();
        argmode = programModeMapper.get_key(argstrl);
        if (argmode == ProgramMode::FORCE)
            throw parsingError();
        this->mode = argmode;
        argi++;

        /// Get dirs paths
        for(; argi < static_cast<uint8_t >(argc); argi++){
            this->paths.emplace_back(deep::deeppath(argv[argi]).normalize_dir());
        }
    }catch(parsingError &Ex){
        /// Report parsing error
        this->parsing_error = true;

        if(throw_error)
            throw Ex;
    }

    this->parsed = !this->parsing_error;

    return *this;
}
