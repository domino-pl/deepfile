//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_PARAMS_H
#define DEEPFILE_PARAMS_H

#include <string>
#include <filesystem>
#include <vector>

#include "modes.h"
#include "../deeptools/tools.h"
#include "../deeptools/deeppath.h"


class parsingError : public dexception::DeepException{
public:
    [[nodiscard]] const char * what() const noexcept override {
        return "Parsing error";
    }
};

#include <iostream>
class ProgramParams{
public:
    bool parsed = false;
    bool parsing_error = false;
    std::string db_path = "";
    std::string output_file = "";
    bool force = false;
    ProgramMode mode = ProgramMode::HELP;
    std::vector<deep::deeppath> paths;
public:

    /// Parse parameters
    /// \param argc - main argc
    /// \param argv - main argv
    /// \param throw_error - enable throwing exceptions
    ProgramParams & parse(const int argc, char **argv, const bool throw_error = false);
};

/// Show program help
void ParamsShowHelp();

std::ostream & operator <<(std::ostream & stream, const ProgramParams & params);

#endif //DEEPFILE_PARAMS_H
