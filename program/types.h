//
// Created by Dominik Przybysz.
//

#ifndef DEEPFILE_TYPES_H
#define DEEPFILE_TYPES_H

typedef uint16_t scan_id_t;
typedef uint16_t level_t;
typedef uint16_t hard_link_count_t;
typedef uint64_t file_size_t;
typedef uint32_t file_id_t;

#endif //DEEPFILE_TYPES_H
